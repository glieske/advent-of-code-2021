import Foundation

public class FileManagerService {
    private let fm: FileManager

    public init() {
        fm = FileManager()
    }

    public func readFileByName(file: String, type: String = "txt", separator: CharacterSet = .newlines) throws -> [String] {
        for bundle in Bundle.allBundles {
            if let path = bundle.path(forResource: file, ofType: type) {
                do {
                    let data = try String(contentsOfFile: path, encoding: .utf8)
                    return data.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: separator)
                } catch {
                    print(error)
                    throw ErrorTypeEnum.runtimeError("read file error")
                }
            }
        }
        throw ErrorTypeEnum.runtimeError("file error")
    }

}
