import Foundation

public class Segment {
    private let p1: Point
    private let p2: Point
    private(set) var isLine: Bool = false
    public var isHorizontal: Bool {
        return p1.y == p2.y
    }
    public var isVertical: Bool {
        return p1.x == p2.x
    }

    public init(start: Point, end: Point) {
        self.p1 = start
        self.p2 = end
    }

    public func getStartPoint() -> Point {
        return self.p1
    }

    public func getEndPoint() -> Point {
        return self.p2
    }

    public func isPartOf(p3: Point) -> Bool {
        let x1 = self.p1.getX().toDouble(), x2 = self.p2.getX().toDouble(), x3 = p3.getX().toDouble()
        let y1 = self.p1.getY().toDouble(), y2 = self.p2.getY().toDouble(), y3 = p3.getY().toDouble()
        let z1 = self.p1.getZ(defaultValue: 1).toDouble(), z2 = self.p2.getZ(defaultValue: 1).toDouble(), z3 = p3.getZ(defaultValue: 1).toDouble()
        let determinant = (x1 * y2 * z3) + (x2 * y3 * z1) + (x3 * y1 * z2) - (x2 * y1 * z3) - (x1 * y3 * z2) - (x3 * y2 * z1)
        if (determinant == 0) {
            if (self.isLine) {
                return true
            } else {
                return min(x1, x2) <= x3 && max(x1, x2) >= x3 && min(y1, y2) <= y3 && max(y1, y2) >= y3 && min(z1, z2) <= z3 && max(z1, z2) >= z3
            }
        } else {
            return false
        }
    }
    
    public func getAllIntegerPoints() -> [Point] {
        var points: [Point] = []
        if (self.isVertical) {
            for y in (min(p1.y, p2.y) ... max(p1.y, p2.y)) {
                points.append(Point(x: p1.x, y: y))
            }
        } else if self.isHorizontal {
            for x in (min(p1.x, p2.x) ... max(p1.x, p2.x)) {
                points.append(Point(x: x, y: p1.y))
            }
        } else {
            /* y = ax + b => a = (y2=y1) / (x2-x1) */
            let slopeX = p2.x - p1.x
            let dx = slopeX.signum() // -1, 0 or 1
            let slopeY = p2.y - p1.y
            let dy = slopeY.signum()
            for i in 0...max(abs(slopeX), abs(slopeY)) {
                points.append(Point(x: i * dx + p1.x, y: i * dy + p1.y))
            }
        }
        return points
    }
    
    public func isOverlappingWithSegment(segment: Segment) -> Bool {
        for s1Point in self.getAllIntegerPoints() {
            for s2Point in segment.getAllIntegerPoints() {
                if s1Point.equals(point: s2Point) {
                    return true
                }
            }
        }
        return false
    }
}
