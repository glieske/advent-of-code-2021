import Foundation

public class Map2D {

    internal var points: [Point] = []

    public init() { }

    private init(points: [Point]) {
        self.points = points
    }

    @discardableResult
    public func addPoint(point: Point) -> Map2D {
        self.points.append(point)
        return self
    }

    @discardableResult
    public func removePoint(point: Point) -> Map2D {
        self.points = self.points.filter { $0 !== point }
        return self
    }

    public func filterByPointValue(value: String?) -> Map2D {
        let pts = self.points.filter { $0.getValue() == value }
        return Map2D(points: pts)
    }

    public func isSet(point: Point) -> Bool {
        return self.points.contains { $0.equals(point: point) }
    }
    
    public func getMinY() throws -> Int64 {
        let minX = self.points.min { $0.getY() > $1.getY() }
        if minX == nil {
            throw ErrorTypeEnum.runtimeError("No entries")
        }
        return minX!.y
    }

    public func getMaxY() throws -> Int64 {
        let maxX = self.points.max { $0.getY() < $1.getY() }
        if maxX == nil {
            throw ErrorTypeEnum.runtimeError("No entries")
        }
        return maxX!.y
    }
    
    public func getMinX() throws -> Int64 {
        let minX = self.points.min { $0.getX() > $1.getX() }
        if minX == nil {
            throw ErrorTypeEnum.runtimeError("No entries")
        }
        return minX!.x
    }

    public func getMaxX() throws -> Int64 {
        let maxX = self.points.max { $0.getX() < $1.getX() }
        if maxX == nil {
            throw ErrorTypeEnum.runtimeError("No entries")
        }
        return maxX!.x
    }
    
    public subscript(x: Int64, y: Int64) -> Point? {
        get {
            let vals = self.points.filter { $0.x == x && $0.y == y }
            if vals.count > 1 {
                fatalError()
            }
            return vals.first
        }
    }
}
