import Foundation

public class Point {
    var x: Int64
    var y: Int64
    var z: Int64?
    var value: String?

    public enum Direction2D {
        case forward, backward, left, right, up, down
    }

    public init(x: Int64, y: Int64, value: String?) {
        self.x = x
        self.y = y
        self.value = value
    }

    public init(x: Int64, y: Int64) {
        self.x = x
        self.y = y
    }

    public init(x: Int64, y: Int64, z: Int64, value: String?) {
        self.z = z
        self.x = x
        self.y = y
        self.value = value
    }

    public init(x: Int64, y: Int64, z: Int64) {
        self.z = z
        self.x = x
        self.y = y
    }

    public func getX() -> Int64 {
        return self.x
    }

    public func getY() -> Int64 {
        return self.y
    }

    public func getZ() -> Int64? {
        return self.z
    }

    public func getZ(defaultValue: Int64) -> Int64 {
        return self.z ?? defaultValue
    }

    public func getValue() -> String? {
        return self.value
    }

    public func getValueForced() -> String {
        return self.value ?? ""
    }

    public func setX(_ v: Int64) -> Point {
        self.x = v
        return self
    }

    public func setY(_ v: Int64) -> Point {
        self.y = v
        return self
    }

    public func setZ(_ v: Int64) -> Point {
        self.z = v
        return self
    }

    public func setValue(_ v: String?) -> Point {
        self.value = v
        return self
    }

    public func translateX(_ x: Int64) -> Point {
        self.x += x
        return self
    }

    public func translateY(_ y: Int64) -> Point {
        self.y += y
        return self
    }

    public func translateZ(_ z: Int64) -> Point {
        if self.z == nil {
            self.z = 1
        }
        self.z! += z
        return self
    }

    public func is3D() -> Bool {
        return self.z != nil
    }

    public func getDistance(point: Point) -> Float64 {
        let xDist: Int64 = point.x - self.x
        let yDist: Int64 = point.y - self.y
        let zDist: Int64 = self.is3D() && point.is3D() ? point.z! - self.z!: 0

        return sqrt(pow(Double(xDist), 2) + pow(Double(yDist), 2) + pow(Double(zDist), 2))
    }

    public func goTo(direction: Direction2D, steps: Int64 = 1) -> Point {
        var newX: Int64 = self.x
        var newY: Int64 = self.y
        var newZ: Int64? = self.z
        switch direction {
        case .forward:
            newY += steps
        case .backward:
            newY -= steps
        case .left:
            newX -= steps
        case .right:
            newY += steps
        case .up:
            newZ = self.getZ(defaultValue: 0) + steps
        case .down:
            newZ = self.getZ(defaultValue: 0) - steps
        }
        if newZ != nil {
            return Point(x: newX, y: newY, z: newZ!)
        } else {
            return Point(x: newX, y: newY)
        }
    }
    
    public func toString() -> String {
        var result = "\(x),\(y)"
        if z != nil {
            result += ",\(z!)"
        }
        return result
    }

    public func equals(point: Point, withValue: Bool = false) -> Bool {
        return self.x == point.getX() && self.y == point.getY() && self.z == point.getZ() && (withValue ? (self.value == point.getValue()) : true)
    }
}
