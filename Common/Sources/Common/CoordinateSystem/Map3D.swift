import Foundation

public class Map3D: Map2D {
    public func getMaxZ() throws -> Int64 {
        var minZ: Int64? = nil
        if self.points.count > 0 {
            for point in self.points {
                if let z = minZ {
                    if z < point.getZ(defaultValue: 0) {
                        minZ = point.getZ(defaultValue: 0)
                    }
                } else {
                    minZ = point.getZ(defaultValue: 0)
                }
            }
        } else {
            throw ErrorTypeEnum.runtimeError("No entries")
        }
        return minZ!
    }
}
