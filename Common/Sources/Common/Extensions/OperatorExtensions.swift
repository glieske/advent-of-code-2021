import Foundation

infix operator ~^
prefix operator >~
 


public prefix func >~ (_ lhs: UInt64) -> UInt64 {
    let delta = lhs.leadingZeroBitCount
    return ((~lhs) << delta) >> delta
}

public func ~^ (_ firstBits: UInt64, _ secondBits: UInt64) -> UInt64
{
    let outputBits = firstBits ^ secondBits
    return >~outputBits
}


@discardableResult public func ~^ <T>(this: T, _ delegate: (T) -> ()) -> T {
    delegate(this)
    return this
}

