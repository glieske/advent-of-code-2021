import Foundation

extension Int {
    func toDouble() -> Double {
        Double(self)
    }

    func toString() -> String {
        "\(self)"
    }
}


extension Int64 {
    func toDouble() -> Double {
        Double(self)
    }

    func toString() -> String {
        "\(self)"
    }
}

extension BinaryInteger {
    public var binaryDescription: String {
        var binaryString = ""
        var internalNumber = self
        var counter = 0

        for _ in (1...self.bitWidth) {
            binaryString.insert(contentsOf: "\(internalNumber & 1)", at: binaryString.startIndex)
            internalNumber >>= 1
            counter += 1
            if counter % 4 == 0 {
                binaryString.insert(contentsOf: " ", at: binaryString.startIndex)
            }
        }

        return binaryString
    }
    public var binarySimple: String {
        return String(self, radix: 2)
    }

    public func trimBits(desiredSize: Self) -> Self {
        let mask = Self(pow(2.0, Double(desiredSize - 1)))
        return Self(self) & mask
    }

    public func isBitSet(_ index: Int) -> Bool {
        (self >> index) & 1 == 1
    }

    public func toBinaryString(padding: Int = 0) -> String {
        String(self, radix: 2).leftPad(with: "0", length: UInt(padding))
    }
}
