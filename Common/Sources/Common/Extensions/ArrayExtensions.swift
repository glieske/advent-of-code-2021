import Foundation


public extension Array where Element: Equatable {
    var unique: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            guard !uniqueValues.contains(item) else { return }
            uniqueValues.append(item)
        }
        return uniqueValues
    }
}

public extension Array where Element: Hashable {

    func next(_ item: Element, nextTimes: Int = 1) -> Element? {
        if let index = self.firstIndex(of: item), index + nextTimes <= self.count {
            let destIndex = index + nextTimes
            return destIndex == self.count ? self[0] : self[destIndex]
        }
        return nil
    }

    func prev(_ item: Element, times: Int = 1) -> Element? {
        if let index = self.firstIndex(of: item), index >= 0 + times {
            return index == 0 ? self.last : self[index - times]
        }
        return nil
    }
}

public extension Array {
    func getValue(at index: Int) -> Element? {
        guard index >= 0 && index < self.count else {
            return nil
        }
        return self[index]
    }
}

extension Array where Element:Equatable {
    public func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
