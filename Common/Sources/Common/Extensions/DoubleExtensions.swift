import Foundation

extension Double {
    func toInt() -> Int {
        Int(self)
    }

    func toInt64() -> Int64 {
        Int64(self)
    }

    func toString() -> String {
        String(format: "%.02f", self)
    }
}
