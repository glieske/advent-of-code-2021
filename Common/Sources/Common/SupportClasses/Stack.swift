import Foundation

public protocol Stackable {
    associatedtype Element
    func peek() -> Element?
    mutating func push(_ element: Element)
    @discardableResult mutating func pop() -> Element?
}

extension Stackable {
    public var isEmpty: Bool { peek() == nil }
}

public struct Stack<Element>: Stackable where Element: Equatable {
    public var storage = [Element]()
    public func peek() -> Element? { storage.last }
    mutating public func push(_ element: Element) { storage.append(element)  }
    mutating public func pop() -> Element? { storage.popLast() }
}

extension Stack: Equatable {
    public static func == (lhs: Stack<Element>, rhs: Stack<Element>) -> Bool { lhs.storage == rhs.storage }
}

extension Stack: CustomStringConvertible {
    public var description: String { "\(storage)" }
}
    
extension Stack: ExpressibleByArrayLiteral {
    public init(arrayLiteral elements: Self.Element...) { storage = elements }
}
