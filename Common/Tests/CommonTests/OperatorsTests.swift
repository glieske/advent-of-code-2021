import XCTest
@testable import Common

final class OperatorsTests: XCTestCase {

    func testLimitedNegation() throws {
        let cases = [
            ["0", "1"],
            ["0101", "1010"],
            ["101010", "010101"]
        ]
        for tc in cases {
            let int = UInt64(tc[0])!
            let neg = >~int
            let str = neg.toBinaryString(padding: tc[0].count)
            XCTAssertEqual(tc[1], str)
        }
    }
}
