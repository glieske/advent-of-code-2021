import Foundation
import AdventTasks

let startTime = CFAbsoluteTimeGetCurrent()

//Task01().run()
//Task02().run()
//Task03().run()
//Task04().run()
//Task05().run()
//Task06().run()
//Task07().run()
//Task08().run()
//Task09().run()
//Task10().run()
//Task11(loadExample: true).run()
Task11().run()

let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
print("Finished in \(timeElapsed) seconds")
