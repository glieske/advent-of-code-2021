import Foundation
import Common

public class Task08: AdventTask {
    override var taskNumber: Int { get { 8 } set { } }

    public override func part1() -> String {
        let outputs: [[String]] = inputFileContent.map { $0.components(separatedBy: " | ") }.map { $0[1].components(separatedBy: " ") }
        
        var results = [Int](repeating: 0, count: 9)
        let lengths = Array(outputs.joined()).map { $0.count }
        results[1] = lengths.filter { elem in elem == 2 }.count
        results[4] = lengths.filter { elem in elem == 4 }.count
        results[7] = lengths.filter { elem in elem == 3 }.count
        results[8] = lengths.filter { elem in elem == 7 }.count
        let result = results.reduce(0, +)
        return "Result is: \(result)"
    }

    public override func part2() -> String {
        let outputs = inputFileContent
            .map { $0.components(separatedBy: " | ") }
            .map { s in
                s
                    .map { r in r.components(separatedBy: " ") }
                    .map { e in e.map { f in String(f.sorted()) }}
            }
        var result = 0
        for data in outputs {
            let test = data[0]
            let output = data[1]
            
            var codemap: [String?] = [String?](repeating: nil, count: 10)
            codemap[1] = test.filter { $0.count == 2 }.first
            codemap[4] = test.filter { $0.count == 4 }.first
            codemap[7] = test.filter { $0.count == 3 }.first
            codemap[8] = "abcdefg"
            
            for num in test {
                if num.count == 5 { // 3, 5 or 2
                    if AContainsB(num, codemap[1]!) { // only 3 fully contains 1 (from this set)
                        codemap[3] = num
                    } else if AContainsB(num, AMinusB(codemap[4]!, codemap[1]!)) { // 4 without right part, remainings are in 5
                        codemap[5] = num
                    } else {
                        codemap[2] = num
                    }
                } else if num.count == 6 { // 9, 6 or 0
                    if !AContainsB(num, codemap[1]!) { // only 6 not fully contain 1, 9 and 0 do
                        codemap[6] = num
                    } else if AContainsB(num, AMinusB(codemap[4]!, codemap[1]!)) { // not 6 and contains a 4 without right part
                        codemap[9] = num
                    } else {
                        codemap[0] = num
                    }
                }
            }
            result += getNumber(encryptionKey: codemap, output: output)
        }
    
        return "Result is: \(result)"
    }
    
    private func AContainsB(_ a: String, _ b: String) -> Bool {
        for char in b {
            if !a.contains(char) {
                return false
            }
        }
        return true
    }
    
    private func AMinusB(_ a: String, _ b: String) -> String {
        return a.filter { !b.contains($0) }
    }
    
    private func getNumber(encryptionKey: [String?], output: [String]) -> Int {
        var result: Int = 0
        var multiplier = 1000
        for digit in output {
            var digitok = false
            for (value, key) in encryptionKey.enumerated() {
                if digit == key {
                    result += multiplier * value
                    multiplier /= 10
                    digitok = true
                }
            }
            if digitok == false {
                fatalError()
            }
        }
        return result
    }
}
