import Foundation

class AimingSimpleSubmarine: SimpleSubmarine {
    private var aim: Int64 = 0

    public override func move(direction: SimpleSubmarine.MovementDirection, steps: Int64) {
        switch direction {
        case .forward:
            translateHorizontalPosition(steps)
            translateDepth(aim * steps)
        case .down:
            aim += steps
        case .up:
            aim -= steps
        }
    }
}
