import Foundation
import Common

class BingoBoard {

    let columnsAmount: Int
    let rowsAmount: Int
    var numbers: [[(value: Int64, hit: Bool)]]

    public init(numbers: [String]) {
        self.numbers = numbers.compactMap {
            $0
                .condenseWhitespace()
                .components(separatedBy: " ")
                .map { (value: Int64($0)!, hit: false) }
        }
        columnsAmount = self.numbers.first!.count
        rowsAmount = numbers.count
    }

    public func hit(_ num: Int64) -> Bool {
        for index in numbers.indices {
            numbers[index] = numbers[index].map { (value: $0.value, hit: $0.hit || $0.value == num) }
        }
        return isWon()
    }

    public func sumWrongNumbers() -> Int64 {
        var sum: Int64 = 0
        for row in numbers.indices {
            sum += numbers[row].filter { $0.hit == false }.map { $0.value }.reduce(0, +)
        }
        return Int64(sum)
    }

    public func isWon() -> Bool {
        var result: Bool = false
        for row in numbers.indices {
            result = result || numbers[row].reduce(true) { resBuf, element in resBuf && element.hit }
            if result { return result }

            var columnValues: [(value: Int64, hit: Bool)] = []
            for column in numbers.indices {
                columnValues += numbers[column].enumerated().filter { index, elem in Int(index) == row }.map { $0.element }
            }
            result = result || columnValues.reduce(true) { resBuf, element in resBuf && element.hit }
            if result { return result }
        }

        return result
    }
}
