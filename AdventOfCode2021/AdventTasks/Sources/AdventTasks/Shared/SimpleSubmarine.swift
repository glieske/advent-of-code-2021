import Foundation
import Common

/*
 X = horizonal position
 Y = vertical position
 Z = depth
 */
class SimpleSubmarine {
    internal let position: Point

    public enum MovementDirection: String {
        case forward = "forward"
        case down = "down"
        case up = "up"
    }

    public init() {
        position = Point(x: 0, y: 0, z: 0)
    }

    public func move(direction: MovementDirection, steps: Int64) {
        switch direction {
        case .forward:
            translateHorizontalPosition(steps)
        case .down:
            translateDepth(steps)
        case .up:
            translateDepth(-steps)
        }
    }

    public func getPosition() -> Point {
        return position
    }
}

extension SimpleSubmarine {
    public func getHorizontalPosition() -> Int64 {
        return self.position.getX()
    }
    public func getVerticalPosition() -> Int64 {
        return self.position.getY()
    }
    public func getDepth() -> Int64 {
        return self.position.getZ()!
    }
    public func translateHorizontalPosition(_ by: Int64) {
        _ = self.position.translateX(by)
    }
    public func translateVerticalPosition(_ by: Int64) {
        _ = self.position.translateY(by)
    }
    public func translateDepth(_ by: Int64) {
        _ = self.position.translateZ(by)
    }
    public func setHorizontalPosition(_ to: Int64) {
        _ = self.position.setX(to)
    }
    public func setVerticalPosition(_ to: Int64) {
        _ = self.position.setY(to)
    }
    public func setDepth(_ to: Int64) {
        _ = self.position.setZ(to)
    }
}
