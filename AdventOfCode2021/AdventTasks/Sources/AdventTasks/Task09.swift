import Foundation
import Common

public class Task09: AdventTask {
    override var taskNumber: Int { get { 9 } set { } }

    public override func part1() -> String {
        let map: [[Int64]] = parse()
        var result: Int64 = 0
        getLowestPointsCoords(map: map).forEach { (x, y) in
            result += map[y][x] + 1
        }

        return "Result is: \(result)"
    }

    public override func part2() -> String {
        let map: [[Int64]] = parse()
        
        let result: Int64 = getLowestPointsCoords(map: map)
            .map { (x, y) in Int64(makeBasin(map: map, x: x, y: y).count) }
            .sorted()
            .suffix(3)
            .reduce(Int64(1), *)

        return "Result is: \(result)"
    }

    private func getLowestPointsCoords(map: [[Int64]]) -> [(Int, Int)] {
        var result: [(Int, Int)] = []
        for y in map.indices {
            for x in map[y].indices {
                let value = map[y][x]
                var adjacentPointValues: [Int64] = []
                getAdjacentPoints(x: x, y: y)
                    .forEach { (x1, y1) in
                    addAdjacentValue(x: x1, y: y1, list: &adjacentPointValues, map: map)
                }
                if adjacentPointValues.filter({ $0 <= value }).count == 0 {
                    result.append((x, y))
                }
            }
        }
        return result
    }

    private func parse() -> [[Int64]] {
        let maxX = inputFileContent.first!.count
        let maxY = inputFileContent.count
        var result: [[Int64]] = Array(repeating: Array(repeating: -1, count: maxX), count: maxY)
        for (y, line) in inputFileContent.enumerated() {
            for (x, pt) in Array(line).enumerated() {
                result[y][x] = Int64(String(pt))!
            }
        }
        return result
    }

    private func getAdjacentPoints(x: Int, y: Int) -> [(Int, Int)] {
        return [
            (x, y - 1),
            (x - 1, y),
            (x + 1, y),
            (x, y + 1)
        ]
    }

    private func addAdjacentValue(x: Int, y: Int, list: inout [Int64], map: [[Int64]]) {
        if map.indices.contains(y) && map.first!.indices.contains(x) {
            list.append(map[y][x])
        }
    }

    private func makeBasin(map: [[Int64]], visited: [(Int, Int)] = [], x: Int, y: Int) -> [(Int, Int)] {
        if map[y][x] == 9 {
            return visited
        }
        var localVisited: [(Int, Int)] = visited + [(x, y)]
        getAdjacentPoints(x: x, y: y)
            .filter { crd in !localVisited.contains(where: { $0 == crd }) }
            .filter { (x1, y1) in map.indices.contains(y1) && map.first!.indices.contains(x1) }
            .forEach { (x1, y1) in
                let basin = makeBasin(map: map, visited: localVisited, x: x1, y: y1)
                for val in basin {
                    if !localVisited.contains(where: { $0 == val }) {
                        localVisited.append(val)
                    }
                }
                
            }
        return localVisited
    }
}
