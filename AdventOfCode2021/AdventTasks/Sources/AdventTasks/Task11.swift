import Foundation
import Common

public class Task11: AdventTask {
    override var taskNumber: Int { get { 11 } set { } }

    public override func part1() -> String {
        var data = parse()
        var flashes: Int64 = 0
        for turn in 1...100 {
            grow(&data)
            glow(&data)
            flashes += countFlashes(data)
            reset(&data)
            //printData(data, turn)
        }
        
        return "Result is: \(flashes)"
    }

    public override func part2() -> String {
        var data = parse()
        var turn: Int64 = 1
        let amount = data.count * data.first!.count
        while true {
            grow(&data)
            glow(&data)
            let flashes = countFlashes(data)
            if (flashes == amount) {
                break
            }
            reset(&data)
            //printData(data, Int(turn))
            turn += 1
        }
        return "Result is: \(turn)"
    }
    
    private func parse() -> [[(Int64, Bool)]] {
        var result: [[(Int64, Bool)]] = Array(repeating: Array(repeating: (-1, false), count: inputFileContent.first!.count), count: inputFileContent.count)
        
        for (y, line) in inputFileContent.enumerated() {
            for (x, pt) in Array(line).enumerated() {
                result[y][x] = (Int64(String(pt))!, false)
            }
        }
        
        return result
    }
    
    private func grow(_ data: inout [[(Int64, Bool)]]) -> Void {
        for (y, line) in data.enumerated() {
            for (x, _) in Array(line).enumerated() {
                data[y][x].0 += 1
            }
        }
    }
    
    private func glow(_ data: inout [[(Int64, Bool)]]) -> Void {
        for (y, line) in data.enumerated() {
            for (x, _) in Array(line).enumerated() {
                if data[y][x].0 > 9 {
                    let adjacentPointsIndexes = getAdjacentPoints(x: x, y: y)
                    for adjacentPointsIndex in adjacentPointsIndexes {
                        if exist(data, x: adjacentPointsIndex.0, y: adjacentPointsIndex.1) {
                            if data[adjacentPointsIndex.1][adjacentPointsIndex.0].1 == false {
                                data[adjacentPointsIndex.1][adjacentPointsIndex.0].0 += 1
                            }
                        }
                    }
                    data[y][x].1 = true
                    data[y][x].0 = 0
                }
            }
        }
        if ableToGlow(data) {
            glow(&data)
        }
    }
    
    private func exist(_ data: [[(Int64, Bool)]], x: Int, y: Int) -> Bool {
        return data.indices.contains(y) && data.first!.indices.contains(x)
    }
    
    private func ableToGlow(_ data: [[(Int64, Bool)]]) -> Bool {
        for (y, line) in data.enumerated() {
            for (x, _) in Array(line).enumerated() {
                if data[y][x].0 > 9 && data[y][x].1 == false {
                    return true
                }
            }
        }
        return false
    }
    
    private func countFlashes(_ data: [[(Int64, Bool)]]) -> Int64 {
        var result: Int64 = 0
        for (y, line) in data.enumerated() {
            for (x, _) in Array(line).enumerated() {
                if data[y][x].1 {
                    result += 1
                }
            }
        }
        return result
    }
    
    private func reset(_ data: inout [[(Int64, Bool)]]) {
        for (y, line) in data.enumerated() {
            for (x, _) in Array(line).enumerated() {
                data[y][x].1 = false
            }
        }
    }
    
    private func printData(_ data: [[(Int64, Bool)]], _ turn: Int) -> Void {
        print("Turn \(turn)")
        for (y, line) in data.enumerated() {
            for (x, _) in Array(line).enumerated() {
                print(String(data[y][x].0), terminator: "")
            }
            print("")
        }
    }
    
    private func getAdjacentPoints(x: Int, y: Int) -> [(Int, Int)] {
        return [
            (x - 1, y - 1),
            (x - 1, y + 1),
            (x, y - 1),
            (x - 1, y),
            (x + 1, y),
            (x, y + 1),
            (x + 1, y - 1),
            (x + 1, y + 1)
        ]
    }

}
