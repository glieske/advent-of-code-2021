import Foundation
import Common

public class Task05: AdventTask {
    override var taskNumber: Int { get { 5 } set { } }


    public override func part1() -> String {
        let segments = parse()
        let points = segments.filter { $0.isVertical || $0.isHorizontal }.flatMap { $0.getAllIntegerPoints() }
        let crossReference = Dictionary(grouping: points, by: { $0.toString() })
        let duplicates = crossReference.filter { $1.count >= 2 }

        return "Result is: \(duplicates.count)"
    }

    public override func part2() -> String {
        let segments = parse()
        let points = segments.flatMap { $0.getAllIntegerPoints() }
        let crossReference = Dictionary(grouping: points, by: { $0.toString() })
        let duplicates = crossReference.filter { $1.count >= 2 }.sorted { $0.1.count > $1.1.count }

        return "Result is: \(duplicates.count)"
    }

    private func parse() -> [Segment] {
        let data = inputFileContent
        var result: [Segment] = []
        for line in data {
            let coordsRaw = line.components(separatedBy: " -> ")
            let p1Raw = coordsRaw[0].components(separatedBy: ",")
            let p1 = Point(x: Int64(p1Raw[0])!, y: Int64(p1Raw[1])!)
            let p2Raw = coordsRaw[1].components(separatedBy: ",")
            let p2 = Point(x: Int64(p2Raw[0])!, y: Int64(p2Raw[1])!)
            let segment = Segment(start: p1, end: p2)
            result.append(segment)
        }
        return result
    }

}
