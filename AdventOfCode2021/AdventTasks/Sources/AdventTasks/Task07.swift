import Foundation
import Common

public class Task07: AdventTask {
    override var taskNumber: Int { get { 7 } set { } }


    public override func part1() -> String {
        let numbers: [Int64] = parse()
        var distances: [Int64] = []
        for crabA in numbers {
            distances.append(numbers.reduce(Int64(0)) {pre, cur in
                pre + abs(cur - crabA)
            })
        }
        let result = distances.min()!
        return "Result is: \(result)"
    }

    public override func part2() -> String {
        let numbers: [Int64] = parse()
        var distances: [Int64] = []
        for i in numbers.min()! ... numbers.max()! {
            distances.append(numbers.reduce(Int64(0)) {pre, cur in
                pre + calcGauss(abs(cur - i))
            })
        }
        
        let result = distances.min()!
        return "Result is: \(result)"
    }
    
    private func calcGauss(_ n: Int64) -> Int64 {
        return n * (n + 1) / 2
    }
    
    /* A second way - calc from 0 to ABS(number - 2)  */
    private func calcSumFromTo(n1: Int64, n2: Int64) -> Int64 {
        if n1 == n2 { return 0 }
        let m1 = min(n1, n2)
        let m2 = max(n1, n2)
        let n:Double = Double(m2-m1+1)
        let result = ((n/2.0) * Double(m1+m2))
        return Int64(result)
    }
    
    private func parse() -> [Int64] {
        return inputFileContent.first!
            .components(separatedBy: ",")
            .map { Int64($0)! }
    }
}
