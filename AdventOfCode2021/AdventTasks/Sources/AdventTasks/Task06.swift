import Foundation
import Common

public class Task06: AdventTask {
    override var taskNumber: Int { get { 6 } set { } }


    public override func part1() -> String {
        let result: UInt64 = simulate(days: 80)
        return "Result is: \(result)"
    }

    public override func part2() -> String {
        let result: UInt64 = simulate(days: 256)
        return "Result is: \(result)"
    }
    
    private func parse() -> [Int8] {
        var result: [Int8] = []
        let input = inputFileContent.first!
        for age in input.components(separatedBy: ",").compactMap({ Int8($0) }) {
            result.append(age)
        }
        return result
    }
    
    private func simulate(days: Int) -> UInt64 {
        let lanternFishes: [Int8] = parse()
        var counters: [UInt64] = [UInt64](repeating: 0, count: 9)
        for i in counters.indices {
            counters[i] = lanternFishes.reduce(UInt64(0)) { pre, cur in pre + (cur == i ? UInt64(1) : UInt64(0)) }
        }
        for _ in 0...days-1 {
            var placeholder: [UInt64] = [UInt64](repeating: 0, count: 9)
            for i in placeholder.indices {
                if i == 0 {
                    placeholder[6] = counters[0]
                    placeholder[8] = counters[0]
                } else {
                    placeholder[i-1] += counters[i]
                }
            }
            counters = placeholder
        }
        let result: UInt64 = counters.reduce(UInt64(0), +)
        return result
    }
}
