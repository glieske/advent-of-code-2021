import Foundation
import Common

public class Task04: AdventTask {
    override var taskNumber: Int { get { 4 } set { } }

    public override func part1() -> String {
        let (boards, numbers) = parse()
        for number in numbers {
            for board in boards {
                if board.hit(number) {
                    let result = number * board.sumWrongNumbers()
                    return "Result is: \(result)"
                }
            }
        }
        fatalError("It shouldn't happen")
    }

    public override func part2() -> String {
        let (boards, numbers) = parse()
        var winners: [BingoBoard] = []
        for number in numbers {
            for board in boards {
                if winners.first(where: { e in e === board }) != nil {
                    continue
                }
                if board.hit(number) {
                    if boards.count - winners.count == 1 {
                        let result = number * board.sumWrongNumbers()
                        return "Result is: \(result)"
                    }
                    winners.append(board)
                }
            }
        }
        fatalError("It shouldn't happen")
    }

    private func parse() -> ([BingoBoard], [Int64]) {
        var input = inputFileContent
        let numbers: [Int64] = input.removeFirst().components(separatedBy: ",").map { Int64($0)! }
        var buffer: [String] = []
        var boards: [BingoBoard] = []
        input.append("")
        for row in input {
            if row == "" {
                if !buffer.isEmpty {
                    let board = BingoBoard(numbers: buffer)
                    boards.append(board)
                    buffer = []
                }
                buffer = []
            } else {
                buffer.append(row)
            }
        }
        return (boards, numbers)
    }

}
