import Foundation
import Common

public class Task01: AdventTask {
    override var taskNumber: Int { get { 1 } set { } }

    public override func part1() -> String {
        var lastNumber: Int64? = nil
        var increases: Int64 = 0
        for number: String in inputFileContent {
            let newNumber = Int64(number)
            if lastNumber != nil {
                increases += newNumber! > lastNumber! ? 1 : 0
            }
            lastNumber = newNumber
        }
        return "It increased \(increases) times"
    }

    public override func part2() -> String {
        var lastNumber: Int64? = nil
        var increases: Int64 = 0
        for (index, number) in inputFileContent.enumerated() {
            if(index + 2 >= inputFileContent.count) {
                break;
            }

            let a = Int64(number)!
            let b = Int64(inputFileContent[index + 1])!
            let c = Int64(inputFileContent[index + 2])!

            let sum = a + b + c

            if lastNumber != nil {
                increases += sum > lastNumber! ? 1 : 0
            }
            lastNumber = sum
        }
        return "It increased \(increases) times"
    }
}
