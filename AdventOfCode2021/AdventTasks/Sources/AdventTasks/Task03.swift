import Foundation
import Common

public class Task03: AdventTask {
    override var taskNumber: Int { get { 3 } set { } }
    private var onesCounter: [Int] = []

    public override func part1() -> String {
        onesCounter = []
        for binaryNumber in inputFileContent {
            if (onesCounter.indices.isEmpty) {
                onesCounter = [Int](repeating: 0, count: binaryNumber.count)
            }
            calculateBitCounter(binaryNumber: binaryNumber)
        }
        let amount = inputFileContent.count
        let gamma = String(Array(onesCounter).map { $0 >= amount - $0 ? "1" : "0" }).binary!
        let epsilon = >~gamma
        let result = gamma * epsilon
        return "Result is: \(result)"
    }

    public override func part2() -> String {
        onesCounter = []
        for binaryNumber in inputFileContent {
            if (onesCounter.indices.isEmpty) {
                onesCounter = [Int](repeating: 0, count: binaryNumber.count)
            }
            calculateBitCounter(binaryNumber: binaryNumber)
        }

        let oxygen = calculateOxygenGeneratorRating()
        let co2 = calculateCO2ScrubberRating()
        let result = oxygen * co2

        return "Result is: \(result)"
    }

    private func calculateBitCounter(binaryNumber: String) {
        for (index, char) in binaryNumber.enumerated() {
            if String(char) == "1" {
                onesCounter[index] += 1
            }
        }
    }

    private func calculateOxygenGeneratorRating() -> UInt64 {
        let set = inputFileContent
        var nums = set.map { $0.binary! }
        let bitsAmount = set.first!.count
        var bitSelector = UInt64(pow(2, Double(bitsAmount - 1)))
        for _ in 0 ..< bitsAmount {
            let onesOnBit = nums.reduce(0) { result, entry in result + (entry & bitSelector == bitSelector ? 1 : 0) }
            if Int(round((Double(nums.count) / 2.0))) <= onesOnBit {
                nums = nums.filter { $0 & bitSelector == 0 }
            } else {
                nums = nums.filter { $0 & bitSelector == bitSelector }
            }
            if nums.count == 1 {
                return nums.first!
            }
            bitSelector >>= 1
        }
        return 0
    }

    private func calculateCO2ScrubberRating() -> UInt64 {
        let set: [String] = inputFileContent
        var nums: [UInt64] = set.compactMap { $0.binary! }
        let bitsAmount: Int = set.first!.count
        var bitSelector = UInt64(pow(2, Double(bitsAmount - 1)))
        for _ in 0 ..< bitsAmount {
            let onesOnBit = nums.reduce(0) { result, entry in result + (entry & bitSelector == bitSelector ? 1 : 0) }
            if Int(round((Double(nums.count) / 2.0))) <= onesOnBit {
                nums = nums.filter { $0 & bitSelector == bitSelector }
            } else {
                nums = nums.filter { $0 & bitSelector == 0 }
            }
            if nums.count == 1 {
                return nums.first!
            }
            bitSelector >>= 1
        }
        return 0
    }


}
