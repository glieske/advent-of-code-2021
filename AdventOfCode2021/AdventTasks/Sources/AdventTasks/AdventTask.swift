import Foundation
import Common

public class AdventTask {
    internal var taskNumber: Int = 0
    internal var input: String = ""
    internal var inputFileName: String?
    internal var inputFileContent: [String] = []
    private let fm: FileManagerService

    internal func getTaskNumber() -> Int {
        return taskNumber
    }

    public init(noFile: Bool = false, loadExample: Bool = false) {
        self.fm = FileManagerService()
        if !noFile {
            let day = String(format: "%02d", getTaskNumber())
            replaceInputFileName(fileName: "InputFiles/day\(day)")
        }
        if loadExample {
            let day = String(format: "%02d", getTaskNumber())
            replaceInputFileName(fileName: "InputFiles/day\(day)-example")
        }
    }

    public init(fileName: String) {
        self.fm = FileManagerService()
        self.inputFileName = fileName
    }

    public func replaceInputFileName(fileName: String) {
        self.inputFileName = fileName
        self.inputFileContent = []
    }

    public func parseInput(fileName: String? = nil) throws {
        if inputFileName == nil && fileName == nil {
            throw ErrorTypeEnum.runtimeError("No file to parse!")
        }

        self.inputFileContent = try! fm.readFileByName(file: inputFileName!)
    }

    public func part1() -> String {
        return "Part 1 is not implemented"
    }

    public func part2() -> String {
        return "Part 2 is not implemented"
    }

    public func run() {
        print("###############################")
        print("Starting Task no. \(taskNumber)")
        try! parseInput()
        print("Result of Part1:")
        print(part1())
        print("Result of Part2:")
        print(part2())
    }
}
