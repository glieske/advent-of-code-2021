import Foundation
import Common

public class Task02: AdventTask {
    override var taskNumber: Int { get { 2 } set { } }

    public override func part1() -> String {
        let submarine = SimpleSubmarine()
        let result = calculate(submarine: submarine)
        return "Result is: \(result)"
    }

    public override func part2() -> String {
        let submarine = AimingSimpleSubmarine()
        let result = calculate(submarine: submarine)
        return "Result is: \(result)"
    }

    private func calculate(submarine: SimpleSubmarine) -> Int64 {
        for instruction: String in inputFileContent {
            let parts = instruction.components(separatedBy: " ").map { String($0) }
            let directionStr: String = parts[0]
            let distance: Int64 = Int64(parts[1])!
            let direction = SimpleSubmarine.MovementDirection(rawValue: directionStr)
            submarine.move(direction: direction!, steps: distance)
        }
        let finalPoint = submarine.getPosition()
        return finalPoint.getX() * finalPoint.getZ()!
    }
}
