import Foundation
import Common

public class Task10: AdventTask {
    override var taskNumber: Int { get { 10 } set { } }
    
    let scoringP1: [Character: Int64] = [
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137
    ]
    
    let scoringP2: [Character: Int64] = [
        ")": 1,
        "]": 2,
        "}": 3,
        ">": 4
    ]

    public override func part1() -> String {
        var result: Int64 = 0
        for line in inputFileContent {
            if let character = getWrongCharacter(line) {
                result += scoringP1[character]!
            }
        }
        
        return "Result is: \(result)"
    }
    
    private func getWrongCharacter(_ line: String) -> Character? {
        var stack = Stack<Character>()
        for char in line {
            switch char {
            case "(", "[", "{", "<":
                stack.push(char)
            case ")", "]", "}", ">":
                let openingOperator = getOpposite(oper: char)
                let lastOpeningOperator = stack.pop()
                if openingOperator != lastOpeningOperator {
                    return char
                }
            default:
                fatalError()
            }
        }
        return nil
    }

    public override func part2() -> String {
        var results: [Int64] = []
        for line in inputFileContent {
            if getWrongCharacter(line) == nil {
                var stack = Stack<Character>()
                for char in line {
                    if scoringP2.keys.contains(getOpposite(oper: char)) {
                        stack.push(char)
                    } else {
                        _ = stack.pop() //it is a correct line so i don't need to check
                    }
                }
                var localScore: Int64 = 0
                while !stack.isEmpty {
                    let char = stack.pop()!
                    localScore *= 5
                    localScore += scoringP2[getOpposite(oper: char)]!
                }
                results.append(localScore)
            }
        }
        
        let result = results.sorted(by: <)[results.count / 2]
        
        return "Result is: \(result)"
    }
    
    private func getOpposite(oper: Character) -> Character {
        switch oper {
        case ")":
            return "("
        case "(":
            return ")"
        case "]":
            return "["
        case "[":
            return "]"
        case "}":
            return "{"
        case "{":
            return "}"
        case ">":
            return "<"
        case "<":
            return ">"
        default:
            fatalError()
        }
    }

    
}
