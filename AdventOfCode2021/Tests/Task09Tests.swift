import XCTest
@testable import AdventTasks


class Task09Tests: XCTestCase {

    var sut: Task09!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = Task09()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testCorrectPart1Result() {
        let correctResult = "Result is: 577"
        try! sut.parseInput()
        XCTAssertEqual(sut.part1(), correctResult, "Part1 result is wrong")
    }

    func testPerformancePart1() {
        self.measure {
            try! sut.parseInput()
            _ = sut.part1()
        }
    }

    func testCorrectPart1ExampleResult() {
        let correctResult = "Result is: 15"
        sut.replaceInputFileName(fileName: sut.inputFileName! + "-example")
        try! sut.parseInput()
        XCTAssertEqual(sut.part1(), correctResult, "Part1 example result is wrong")
    }

    func testCorrectPart2Result() {
        let correctResult = "Result is: 1069200"
        try! sut.parseInput()
        XCTAssertEqual(sut.part2(), correctResult, "Part2 result is wrong")
    }

    func testPerformancePart2() {
        self.measure {
            try! sut.parseInput()
            _ = sut.part2()
        }
    }

    func testCorrectPart2ExampleResult() {
        let correctResult = "Result is: 1134"
        sut.replaceInputFileName(fileName: sut.inputFileName! + "-example")
        try! sut.parseInput()
        XCTAssertEqual(sut.part2(), correctResult, "Part1 example result is wrong")
    }
}
