import XCTest
@testable import AdventTasks


class Task01Tests: XCTestCase {

    var sut: Task01!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = Task01()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testCorrectPart1Result() {
        let correctResult = "It increased 1583 times"
        try! sut.parseInput()
        XCTAssertEqual(sut.part1(), correctResult, "Part1 result is wrong")
    }

    func testPerformancePart1() {
        self.measure {
            try! sut.parseInput()
            _ = sut.part1()
        }
    }

    func testCorrectPart1ExampleResult() {
        let correctResult = "It increased 7 times"
        sut.replaceInputFileName(fileName: sut.inputFileName! + "-example")
        try! sut.parseInput()
        XCTAssertEqual(sut.part1(), correctResult, "Part1 example result is wrong")
    }

    func testCorrectPart2Result() {
        let correctResult = "It increased 1627 times"
        try! sut.parseInput()
        XCTAssertEqual(sut.part2(), correctResult, "Part2 result is wrong")
    }

    func testPerformancePart2() {
        self.measure {
            try! sut.parseInput()
            _ = sut.part2()
        }
    }

    func testCorrectPart2ExampleResult() {
        let correctResult = "It increased 5 times"
        sut.replaceInputFileName(fileName: sut.inputFileName! + "-example")
        try! sut.parseInput()
        XCTAssertEqual(sut.part2(), correctResult, "Part2 example result is wrong")
    }
}
