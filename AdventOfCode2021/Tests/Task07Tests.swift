import XCTest
@testable import AdventTasks


class Task07Tests: XCTestCase {

    var sut: Task07!

    override func setUpWithError() throws {
        try super.setUpWithError()
        sut = Task07()
    }

    override func tearDownWithError() throws {
        sut = nil
        try super.tearDownWithError()
    }

    func testCorrectPart1Result() {
        let correctResult = "Result is: 364898"
        try! sut.parseInput()
        XCTAssertEqual(sut.part1(), correctResult, "Part1 result is wrong")
    }

    func testPerformancePart1() {
        self.measure {
            try! sut.parseInput()
            _ = sut.part1()
        }
    }

    func testCorrectPart1ExampleResult() {
        let correctResult = "Result is: 37"
        sut.replaceInputFileName(fileName: sut.inputFileName! + "-example")
        try! sut.parseInput()
        XCTAssertEqual(sut.part1(), correctResult, "Part1 example result is wrong")
    }

    func testCorrectPart2Result() {
        let correctResult = "Result is: 104149091"
        try! sut.parseInput()
        XCTAssertEqual(sut.part2(), correctResult, "Part2 result is wrong")
    }

    func testPerformancePart2() {
        self.measure {
            try! sut.parseInput()
            _ = sut.part2()
        }
    }

    func testCorrectPart2ExampleResult() {
        let correctResult = "Result is: 168"
        sut.replaceInputFileName(fileName: sut.inputFileName! + "-example")
        try! sut.parseInput()
        XCTAssertEqual(sut.part2(), correctResult, "Part1 example result is wrong")
    }
}
